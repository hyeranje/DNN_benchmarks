import numpy as np
import csv
import matplotlib.pyplot as plt
from PIL import Image
caffe_root = '../'  # this file is expected to be in {caffe_root}/examples
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe
caffe.set_mode_cpu()
net = caffe.Net('./alexnet/alexnet.prototxt',
                './alexnet/bvlc_alexnet.caffemodel',
                caffe.TEST)
#imagemean_file = '/home/aajna/ra_project/caffe/python/caffe/imagenet/ilsvrc_2012_mean.npy'
transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
#transformer.set_mean('data', np.load(imagemean_file).mean(1).mean(1))
transformer.set_transpose('data', (2,0,1))
transformer.set_raw_scale('data', 255.0)
net.blobs['data'].reshape(1,3,227,227)
img = caffe.io.load_image('../python/cat.jpg')
net.blobs['data'].data[...] = transformer.preprocess('data', img)
output = net.forward()
with open('weights_conv1.txt','wb') as csvfile:
    matrixwriter = csv.writer(csvfile, delimiter=' ')
    for row in net.params['conv1'][0].data:
	for value in row:
    	    np.savetxt(csvfile, value, delimiter='\n');
with open('weights_conv2.txt','wb') as csvfile:
    matrixwriter = csv.writer(csvfile, delimiter=' ')
    for row in net.params['conv2'][0].data:
	for value in row:
    	    np.savetxt(csvfile, value, delimiter='\n');
with open('weights_conv3.txt','wb') as csvfile:
    matrixwriter = csv.writer(csvfile, delimiter=' ')
    for row in net.params['conv3'][0].data:
	for value in row:
    	    np.savetxt(csvfile, value, delimiter='\n');
with open('weights_conv4.txt','wb') as csvfile:
    matrixwriter = csv.writer(csvfile, delimiter=' ')
    for row in net.params['conv4'][0].data:
	for value in row:
    	    np.savetxt(csvfile,value, delimiter='\n');
with open('weights_conv5.txt','wb') as csvfile:
    matrixwriter = csv.writer(csvfile, delimiter=' ')
    for row in net.params['conv5'][0].data:
	for value in row:
    	    np.savetxt(csvfile,value, delimiter='\n');
#with open('weights_fc8.txt','wb') as csvfile:
#    matrixwriter = csv.writer(csvfile, delimiter=' ')
#    for row in net.params['fc8'][0].data:
#	for value in row:
#            print value
#with open('bias8.txt','wb') as csvfile:
#    matrixwriter = csv.writer(csvfile, delimiter=' ')
#Bias values
#    matrixwriter.writerow(net.params['fc8'][1].data)
csvfile.close()
         #matrixwriter.writerow(row + '\n')
	 #matrixwriter.writerow('\n')
with open('conv1_out.txt', 'w') as f:
    for row in  net.blobs['conv1'].data[0]:
            for value in row:
                np.savetxt(f, value, fmt='%f', delimiter='\n')
#with open('norm1.txt', 'w') as f:
#    for row in  net.blobs['norm1'].data[0]:
#            for value in row:
#                np.savetxt(f, value, fmt='%f', delimiter='\n')
with open('norm1.txt', 'w') as f:
    for row in  net.blobs['norm1'].data[0]:
            for value in row:
                np.savetxt(f, value, fmt='%f', delimiter='\n')
with open('pool1.txt', 'w') as f:
    for row in  net.blobs['pool1'].data[0]:
            for value in row:
                np.savetxt(f, value, fmt='%f', delimiter='\n')
with open('conv2_out.txt', 'w') as f:
    for row in  net.blobs['conv2'].data[0]:
            for value in row:
                np.savetxt(f, value, fmt='%f', delimiter='\n')
with open('pool2.txt', 'w') as f:
    for row in  net.blobs['pool2'].data[0]:
            for value in row:
                np.savetxt(f, value, fmt='%f', delimiter='\n')
with open('conv3_out.txt', 'w') as f:
    for row in  net.blobs['conv3'].data[0]:
            for value in row:
                np.savetxt(f, value, fmt='%f', delimiter='\n')
with open('conv4_out.txt', 'w') as f:
    for row in  net.blobs['conv4'].data[0]:
            for value in row:
                np.savetxt(f, value, fmt='%f', delimiter='\n')
with open('conv5_out.txt', 'w') as f:
    for row in  net.blobs['conv5'].data[0]:
            for value in row:
                np.savetxt(f, value, fmt='%f', delimiter='\n')
with open('pool5.txt', 'w') as f:
    for row in  net.blobs['pool5'].data[0]:
            for value in row:
                np.savetxt(f, value, fmt='%f', delimiter='\n')
with open('prob_out.txt', 'w') as f:
    for row in  net.blobs['prob'].data[0]:
        print row
prediction = net.predict('../python/cat.jpg')
print 'predicted classes:',prediction[0].argmax()
